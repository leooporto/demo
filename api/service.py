from flask import Flask, request, jsonify
from model.ship import Ship


app = Flask(__name__)

def get_rate(ship):
    return ship.get('hyperdrive')

@app.route('/ships', methods=['POST'])
def list_ships():
    print("Getting ships order by rate")
    ship_list = []
    
    ships = Ship.get_all()
    for ship in ships.tuples().iterator():
        par = {"name":ship[1], "hyperdrive":ship[2]}
        ship_list.append(par)
        
    #order
    ship_list.sort(key=get_rate, reverse=False)
    return jsonify(ship_list)
    

    

    
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80)