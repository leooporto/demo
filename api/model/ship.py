import peewee as pw
from dbconn import DBConn
#from service import myDB

class Ship(pw.Model):
    class Meta:
        database = DBConn.get_connector()
        db_table = 'ship'
   
    id = pw.IntegerField(primary_key=True)
    name = pw.CharField()
    hyperdrive = pw.FloatField()
    
    @staticmethod
    def set_db(db):
        myDB = db
    
    @staticmethod
    def get_all():
        return Ship.select()