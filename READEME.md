## This is a step by step guide if you want to run docker and have fun with default setup.

### Build with docker
Within demo folder, run
```
docker build .
```
then run
```
docker run -p 80:80 ID_IMAGE_CREATED
```


### RUNNIG THE API

#### Listing all ships ordered by their hyperdive rating
```
POST localhost/ships

```
-->
```
[
    {
        "hyperdrive": 1.0,
        "name": "x-wing"
    },
    {
        "hyperdrive": 2.0,
        "name": "tie-fighter"
    }
]
```

