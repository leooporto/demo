FROM ubuntu


RUN apt-get update
RUN apt-get install -y python3.8
RUN apt-get install -y python3-pip
RUN pip3 install flask
RUN pip3 install pymysql
RUN pip3 install peewee


COPY api/ /api/

CMD ["/usr/bin/python3","/api/service.py"]
EXPOSE 80

